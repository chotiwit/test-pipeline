// TestTestHello123.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe("My First Test", function() {
  it("Open site", function() {
    cy.visit("temp-test-a06ae.web.app");
    expect(true).to.equal(true);
  });
  it("Dose not do much", function() {
    expect(true).to.equal(true);
  });
});
describe("My 2nd Test", function() {
  it("Open site", function() {
    cy.visit("temp-test-a06ae.web.app");
    expect(true).to.equal(false);
  });
  it("Dose not do much", function() {
    expect(true).to.equal(true);
  });
});
