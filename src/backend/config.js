export const config = {
  user: process.env.VUE_APP_DB_USER,
  host: process.env.VUE_APP_DB_HOST,
  pass: process.env.VUE_APP_DB_PASS,
};
export default config;
